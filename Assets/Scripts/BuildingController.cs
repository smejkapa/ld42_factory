﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class BuildingController : MonoBehaviour
    {
        public SpriteRenderer[] SpritesToColor;

        public bool IsIndestructible { get; set; }
        public BuildingData BuildingData { get; set; }
        public Vector2Int BuildingOrigin { get; set; }

        public void SetHighlight(Color highlight)
        {
            foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
            {
                spriteRenderer.color = highlight;
            }
        }

        public void ResetHighlight()
        {
            for (var i = 0; i < _spriteRenderers.Length; i++)
            {
                SpriteRenderer spriteRenderer = _spriteRenderers[i];
                spriteRenderer.color = _originalColors[i];
            }
        }

        public virtual void Commit()
        {

        }


        #region Private

        private SpriteRenderer[] _spriteRenderers;
        private List<Color> _originalColors;

        protected virtual void Start()
        {
            foreach (SpriteRenderer spriteRenderer in SpritesToColor)
            {
                spriteRenderer.color = BuildingData.BuildingColor;
            }

            _spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            _originalColors = new List<Color>();
            foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
            {
                _originalColors.Add(spriteRenderer.color);
            }
        }

        #endregion
    }
}
