﻿using UnityEngine;

namespace Scripts
{
    public enum BuildingType
    {
        None,
        Path,
        Factory,
        Mine,
        Shop,
        Storage
    }

    public enum BuildingId
    {
        None = 0,
        Road = 1,
        Storage = 3,

        ClayMine = 100,
        BranchMine = 101,
        FlowerMine = 102,
        BugMine = 103,
        BerriesMine = 104,

        StatueFactory = 200,
        LeafStickFactory = 201,
        BrickFactory = 202,
        CupFactory = 203,
        WingMeatFactory = 204,
        WaterFactory = 205,
        BasketFactory = 206,
        FoodFactory = 207,
        PillowFactory = 208,
        BlanketFactory = 209,
        BedFactory = 210,
        FowerPotFactory = 211,

        StatueShop = 500,
        BedShop = 501,
        FlowerPotShop = 502,
        FoodShop = 503,


    }

    [CreateAssetMenu(fileName = "Building", menuName = "AntFactory/Building", order = 1)]
    public class BuildingData : ScriptableObject
    {
        public BuildingId Id;
        public BuildingType BuildingType;
        public int Width;
        public int Height;
        public Sprite Sprite;
        public GameObject BuildingPrefab;
        public Color BuildingColor;
        public int Cost;
        public bool StayInBuildMode;
        public Product[] Consumes;
        public Product[] Produces;
        public int GoldProduction;
    }
}
