﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class FactoryItem : MonoBehaviour
    {
        public Product Product
        {
            get { return _product; }
            set
            {
                _spriteRenderer.sprite = value?.Sprite;
                _product = value;
            }

        }
        public bool IsReady => _progress >= 1.0f;
        public bool IsInProgress { get; private set; }

        public void ConsumeProduct()
        {
            _progress = 0.0f;
            IsInProgress = false;
        }

        public void StartNewProduction(Action onFinishedCallback)
        {
            _progress = 0.0f;
            IsInProgress = true;
            _onFinishedCallback = onFinishedCallback;
        }

        public void ReceiveProduct()
        {
            _progress = 1.0f;
            IsInProgress = false;
        }


        #region Private
        private float _progress;
        private Product _product;

        private Material _material;
        private int _progressId;
        private int _isActiveId;

        private SpriteRenderer _spriteRenderer;

        private Action _onFinishedCallback;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();

            _material = _spriteRenderer.material;
            _progressId = Shader.PropertyToID("_Progress");
            _isActiveId = Shader.PropertyToID("_IsActive");
        }

        private void Update()
        {
            if (GameManager.Instance.IsPaused)
            {
                return;
            }

            if (IsInProgress)
            {
                if (Product.SecondsToProduce > 0)
                {
                    _progress += Time.deltaTime / Product.SecondsToProduce;
                }
                else // Product should be produced instantly
                {
                    _progress = 1.0f;
                }

                if (_progress >= 1.0f)
                {
                    // Production just finished
                    _progress = 1.0f;
                    IsInProgress = false;

                    Debug.Assert(_onFinishedCallback != null);
                    _onFinishedCallback();
                    _onFinishedCallback = null;
                }
            }

            _material.SetFloat(_progressId, _progress);
        }

        #endregion
    }
}
