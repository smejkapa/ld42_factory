﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class Level : MonoBehaviour
    {
        public int Width;
        public int Height;

        public static readonly float TILE_DIMENSIONS = 0.64f;

        public int StartingGold;
        public GameObject BuildableTile;
        public Text GoldText;
        public GameObject FlyingTextPrefab;

        public GameObject[] WinConditionPanels;

        public Button[] BuildingButtons;

        public StorageManager StorageManager { get; }

        public Tile[,] Tiles { get; set; }

        public Level()
        {
            StorageManager = new StorageManager(this);
        }

        public void PlaceBuilding(BuildingData building, Vector2Int position)
        {
            Debug.Assert(CanBuild(building, position));

            CurrentGold -= building.Cost;

            var buildingController = DoBuildingPlacement(building, position);

            CreateFlyingText(-building.Cost, PositionUtil.TileToCenterWorld(position));

            if (GameManager.Instance.GameMode == GameMode.Pause)
            {
                _uncommitedBuildings.Add(buildingController);
            }
            else if (GameManager.Instance.GameMode == GameMode.Play)
            {
                GameManager.Instance.ProductionManager.MatchProducersAndConsumers();
            }
        }

        public bool CanBuild(BuildingData building, Vector2Int position)
        {
            if (building.Cost > CurrentGold)
            {
                return false;
            }

            for (int x = position.x; x < position.x + building.Width; ++x)
            {
                for (int y = position.y; y < position.y + building.Height; ++y)
                {
                    if (!IsInBounds(x, y) || !Tiles[x, y].IsBuildable)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void HighlightTileForDestroy(Vector2Int position)
        {
            // Cancel previous highlight
            ResetHighlight();

            if (!IsInBounds(position))
            {
                return;
            }

            Tile tile = Tiles[position.x, position.y];
            if (tile.Building == null || 
                tile == _highlightedTile ||
                tile.Building.IsIndestructible)
            {
                return;
            }
            
            // Set new highlight
            //tile.Building.GetComponent<SpriteRenderer>().color = Color.red;
            tile.Building.SetHighlight(Color.red);
            _highlightedTile = tile;
        }

        public void DestroyBuilding(Vector2Int position)
        {
            // Don't destroy anything on empty tiles
            if (!IsInBounds(position) || 
                Tiles[position.x, position.y].Building == null ||
                Tiles[position.x, position.y].Building.IsIndestructible)
            {
                return;
            }

            Tile tile = Tiles[position.x, position.y];
            Debug.Assert(tile == _highlightedTile);

            ResetHighlight();

            // Get necessary data and destroy underlying gameobject
            BuildingController buildingController = tile.Building;
            BuildingData buildingData = buildingController.BuildingData;
            Vector2Int buildingOrigin = buildingController.BuildingOrigin;
            Destroy(tile.Building.gameObject);

            // Reset all tiles where buildig was
            for (int x = buildingOrigin.x; x < buildingOrigin.x + buildingData.Width; ++x)
            {
                for (int y = buildingOrigin.y; y < buildingOrigin.y + buildingData.Height; ++y)
                {
                    Tiles[x, y].Building = null;
                }
            }

            _buildings.Remove(buildingController);

            // In pause mode refund the building
            if (GameManager.Instance.GameMode == GameMode.Pause)
            {
                if (_uncommitedBuildings.Contains(buildingController))
                {
                    CurrentGold += buildingData.Cost;
                    CreateFlyingText(buildingData.Cost, PositionUtil.TileToCenterWorld(buildingController.BuildingOrigin));
                }
            }
        }

        public bool IsInBounds(Vector2Int position)
        {
            return IsInBounds(position.x, position.y);
        }

        public BuildingController GetBuildingAt(Vector2Int position)
        {
            return Tiles[position.x, position.y].Building;
        }

        public void SellProduct(int amount, ProductType product, Vector2Int position)
        {
            CurrentGold += amount;
            CreateFlyingText(amount, PositionUtil.TileToCenterWorld(position));
            if (_victoryConditions.ContainsKey(product) && 
                _victoryConditions[product].Item1 < _victoryConditions[product].Item2)
            {
                _victoryConditions[product] = Tuple.Create(_victoryConditions[product].Item1 + 1, _victoryConditions[product].Item2);
                var keys = _victoryConditions.Keys.ToList();

                WinConditionPanels[keys.IndexOf(product)].GetComponentInChildren<Text>().text =
                    $"{_victoryConditions[product].Item1}/{_victoryConditions[product].Item2}";

                if (IsVictory())
                {
                    GameManager.Instance.Victory();
                }
            }
        }

        public LevelSave GenerateSave()
        {
            var save = new LevelSave
            {
                StartingGold = StartingGold,
                Width = Width,
                Height = Height,
                Buildings = new List<BuildingPositionPair>()
            };

            foreach (var building in _buildings)
            {
                save.Buildings.Add(new BuildingPositionPair
                {
                    BuildingId = (int)building.BuildingData.Id,
                    Position = building.BuildingOrigin
                });
            }

            if (_victoryConditions != null)
            {
                save.VictoryConditions = new List<VictoryCondition>();
                foreach (var victoryCondition in _victoryConditions)
                {
                    save.VictoryConditions.Add(new VictoryCondition
                    {
                        Count = victoryCondition.Value.Item2,
                        Product = (int) victoryCondition.Key
                    });
                }
            }

            if (_availableBuildings != null)
            {
                save.AvailableBuildings = new List<int>();
                foreach (BuildingId availableBuilding in _availableBuildings)
                {
                    save.AvailableBuildings.Add((int) availableBuilding);
                }
            }

            return save;
        }

        public void LoadFromSave(LevelSave save)
        {
            Reset();

            Width = save.Width;
            Height = save.Height;
            GenerateTiles();

            foreach (BuildingPositionPair buildingPositionPair in save.Buildings)
            {
                var building = GameManager.Instance.ProductionManager.Buildings[(BuildingId)buildingPositionPair.BuildingId];
                var buildingController = DoBuildingPlacement(building, buildingPositionPair.Position);
                buildingController.IsIndestructible = true;
            }

            if (save.VictoryConditions != null)
            {
                _victoryConditions = new Dictionary<ProductType, Tuple<int, int>>();
                foreach (VictoryCondition saveVictoryCondition in save.VictoryConditions)
                {
                    _victoryConditions.Add((ProductType)saveVictoryCondition.Product, Tuple.Create(0, saveVictoryCondition.Count));
                }

                var keys = _victoryConditions.Keys.ToList();
                for (int i = 0; i < WinConditionPanels.Length; ++i)
                {
                    if (i >= _victoryConditions.Count)
                    {
                        WinConditionPanels[i].SetActive(false);
                    }
                    else
                    {
                        var product = GameManager.Instance.ProductionManager.Products[keys[i]];
                        var images = WinConditionPanels[i].GetComponentsInChildren<Image>();
                        foreach (Image image in images)
                        {
                            if (image.transform != WinConditionPanels[i].transform)
                            {
                                image.sprite = product.Sprite;
                            }
                        }

                        WinConditionPanels[i].GetComponentInChildren<Text>().text =
                            $"{_victoryConditions[keys[i]].Item1}/{_victoryConditions[keys[i]].Item2}";
                    }
                }
            }

            if (save.AvailableBuildings != null)
            {
                foreach (var button in BuildingButtons)
                {
                    var id = button.gameObject.GetComponent<BuildingIdHolder>().BuildingId;
                    var isEnabled = save.AvailableBuildings.Contains((int) id);
                    button.interactable = isEnabled;
                    button.gameObject.transform.GetChild(0).gameObject.SetActive(isEnabled);
                }
            }

            StartingGold = save.StartingGold;
            CurrentGold = save.StartingGold;
        }

        public IEnumerable<StorageController> GetStorages()
        {
            return _buildings.OfType<StorageController>();
        }


        #region Private

        private int _currentGold;
        private int CurrentGold
        {
            get { return _currentGold; }
            set
            {
                _currentGold = value;
                GoldText.text = $"{value}";
            }
        }

        private GameObject _tilesWrapper;
        private Tile _highlightedTile;
        private readonly List<BuildingController> _uncommitedBuildings = new List<BuildingController>();
        private readonly List<BuildingController> _buildings = new List<BuildingController>();

        private List<BuildingId> _availableBuildings = null;
        private Dictionary<ProductType, Tuple<int, int>> _victoryConditions = new Dictionary<ProductType, Tuple<int, int>>();

        private void Awake()
        {
            GenerateTiles();
        }

        private void Reset()
        {
            if (_tilesWrapper != null)
            {
                Destroy(_tilesWrapper);
            }

            _buildings.Clear();
            _uncommitedBuildings.Clear();
            ResetHighlight();
            _victoryConditions = new Dictionary<ProductType, Tuple<int, int>>();
        }

        private void GenerateTiles()
        {
            _tilesWrapper = new GameObject("Tiles wrapper");
            Tiles = new Tile[Width, Height];

            for (int x = 0; x < Width; ++x)
            {
                for (int y = 0; y < Height; ++y)
                {
                    GameObject newTile = Instantiate(BuildableTile);
                    newTile.transform.position = new Vector3(x * TILE_DIMENSIONS, y * TILE_DIMENSIONS);
                    newTile.transform.parent = _tilesWrapper.transform;
                    Tiles[x, y] = new Tile
                    {
                        TileObject = newTile
                    };
                }
            }
        }

        private void Start()
        {
            CurrentGold = StartingGold;
            GameManager.Instance.OnInputModeChanged += HandleInputModeChange;
            GameManager.Instance.OnGameModeChanged += HandleGameModeChange;
        }

        private void HandleInputModeChange(InputMode oldMode, InputMode newMode, object args)
        {
            if (newMode != InputMode.Destroy)
            {
                ResetHighlight();
            }
        }

        private void HandleGameModeChange(GameMode oldMode, GameMode newMode, object args)
        {
            if (newMode == GameMode.Play)
            {
                if (_uncommitedBuildings.Count > 0)
                {
                    GameManager.Instance.ProductionManager.MatchProducersAndConsumers();
                }
                CommitBuildings();
            }
        }

        private void CommitBuildings()
        {
            foreach (BuildingController uncommitedBuilding in _uncommitedBuildings)
            {
                uncommitedBuilding.Commit();
            }
            _uncommitedBuildings.Clear();
        }

        private void Update()
        {
        }

        private bool IsVictory()
        {
            foreach (var victoryCondition in _victoryConditions.Values)
            {
                if (victoryCondition.Item1 < victoryCondition.Item2)
                {
                    return false;
                }
            }

            return true;
        }

        private void ResetHighlight()
        {
            _highlightedTile?.Building?.ResetHighlight();
            _highlightedTile = null;
        }

        private BuildingController DoBuildingPlacement(BuildingData building, Vector2Int position)
        {
            GameObject buildingGo = Instantiate(building.BuildingPrefab);
            buildingGo.transform.position = PositionUtil.TileToWorld(position);
            buildingGo.transform.parent = _tilesWrapper.transform;

            var buildingController = buildingGo.GetComponent<BuildingController>();
            buildingController.BuildingData = building;
            buildingController.BuildingOrigin = position;

            for (int x = position.x; x < position.x + building.Width; ++x)
            {
                for (int y = position.y; y < position.y + building.Height; ++y)
                {
                    Tiles[x, y].Building = buildingController;
                }
            }

            _buildings.Add(buildingController);

            return buildingController;
        }

        private void CreateFlyingText(int cost, Vector3 position)
        {
            GameObject textObject = Instantiate(FlyingTextPrefab);
            textObject.transform.position = position;

            var textController = textObject.GetComponent<FlyingTextController>();
            textController.SetText(cost >= 0 ? $"+{cost}" : cost.ToString());
            textController.SetTextColor(cost < 0 ? Color.red : Color.yellow);
        }

        private bool IsInBounds(int x, int y)
        {
            return 
                x >= 0 && 
                x < Width && 
                y >= 0 && 
                y < Height;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnInputModeChanged -= HandleInputModeChange;
            GameManager.Instance.OnGameModeChanged -= HandleGameModeChange;
        }

        #endregion
    }
}
