﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public static class PositionUtil
    {
        public static Vector3 TileToWorld(Vector2Int tilePosition)
        {
            return new Vector3(Level.TILE_DIMENSIONS * tilePosition.x, Level.TILE_DIMENSIONS * tilePosition.y);
        }

        public static Vector3 TileToCenterWorld(Vector2Int tilePosition)
        {
            return new Vector3(
                Level.TILE_DIMENSIONS * (tilePosition.x + 0.5f), 
                Level.TILE_DIMENSIONS * (tilePosition.y + 0.5f)
            );
        }

        public static Vector2Int WorldToTile(Vector3 worldPosition)
        {
            return new Vector2Int(
                Mathf.FloorToInt(worldPosition.x / Level.TILE_DIMENSIONS),
                Mathf.FloorToInt(worldPosition.y / Level.TILE_DIMENSIONS)
            );
        }

        public static Vector2Int GetTileAtCursor()
        {
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var cursorPos = new Vector2Int(
                Mathf.FloorToInt(worldPos.x / Level.TILE_DIMENSIONS),
                Mathf.FloorToInt(worldPos.y / Level.TILE_DIMENSIONS)
            );
            return cursorPos;
        }

        public static IComparer<Vector2Int> PositionComparer = Comparer<Vector2Int>.Create((a, b) =>
        {
            if (a.x < b.x)
                return -1;
            if (a.x > b.x)
                return 1;
            if (a.y > b.y)
                return -1;
            if (a.y < b.y)
                return 1;
            return 0;
        });
    }
}
