﻿using UnityEngine;

namespace Scripts
{
    public class Builder : MonoBehaviour
    {
        public Color CanBuildColor;
        public Color CannotBuildColor;

        #region Private

        private BuildingData _currentBuilding;
        private SpriteRenderer _currentSprite;

        private void Awake()
        {
            _currentSprite = gameObject.AddComponent<SpriteRenderer>();
            _currentSprite.sortingLayerName = "ToBuild";
        }

        private void Start()
        {
            GameManager.Instance.OnInputModeChanged += HandleInputModeChange;
        }

        private void HandleInputModeChange(InputMode oldMode, InputMode newMode, object args)
        {
            if (newMode == InputMode.Build)
            {
                SetCurrentBuilding((BuildingData)args);
            }
            else
            {
                CancelBuildingMode();
            }
        }

        private void SetCurrentBuilding(BuildingData building)
        {
            _currentBuilding = building;
            _currentSprite.sprite = building.Sprite;
        }

        void CancelBuildingMode()
        {
            _currentBuilding = null;
            _currentSprite.sprite = null;
        }

        private void Update()
        {
            InputMode inputMode = GameManager.Instance.InputMode;
            if (inputMode == InputMode.None)
            {
                return;
            }

            // Find closest tile to cursor
            var cursorPos = PositionUtil.GetTileAtCursor();

            if (inputMode == InputMode.Build)
            {
                HandleBuildMode(cursorPos);
            }
            else if (inputMode == InputMode.Destroy)
            {
                HandleDestroyMode(cursorPos);
            }
        }

        private void HandleBuildMode(Vector2Int cursorPos)
        {
            // Set sprite position on cursor
            transform.position =
                new Vector2(cursorPos.x * Level.TILE_DIMENSIONS, cursorPos.y * Level.TILE_DIMENSIONS);

            // Check if tiles below the building are empty
            Level level = GameManager.Instance.Level;
            bool canBuild = level.CanBuild(_currentBuilding, cursorPos);
            _currentSprite.color = canBuild ? CanBuildColor : CannotBuildColor;

            // Place the building
            if (Input.GetButton("SubmitGame") && canBuild)
            {
                level.PlaceBuilding(_currentBuilding, cursorPos);
                if (!_currentBuilding.StayInBuildMode)
                {
                    GameManager.Instance.ResetInputMode();
                }
            }
        }

        private void HandleDestroyMode(Vector2Int cursorPos)
        {
            Level level = GameManager.Instance.Level;
            level.HighlightTileForDestroy(cursorPos);

            // Destroy the building
            if (Input.GetButtonDown("SubmitGame"))
            {
                level.DestroyBuilding(cursorPos);
            }
        }

        private void OnDisable()
        {
            GameManager.Instance.OnInputModeChanged -= HandleInputModeChange;
        }

        #endregion
    }
}
