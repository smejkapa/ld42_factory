﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scripts;

namespace Scripts
{
    public interface IProductBuilding
    {
        void ReceiveMaterial(ProductType materialType);
        void RemoveProduct(ProductType productType);
        bool IsStorage { get; }
        event Action OnDestroyed;
    }
}
