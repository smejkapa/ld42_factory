﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Scripts
{
    public enum ProductType
    {
        None = 0,
        Leaf = 1,
        QueenStatue = 2,
        Gold = 3,
        Bug = 4,
        Branch = 5,
        Flower = 6,
        Stick = 7,
        Berries = 8,
        Brick = 9,
        FlowerPot = 10,
        Cup = 11,
        Wings = 12,
        Pillow = 13,
        Blanket = 14,
        Bed = 15,
        Meat = 16,
        Basket = 17,
        Food = 18,
        Water = 19,
        Clay = 20
    }

    public class ProductionManager : MonoBehaviour
    {
        public GameObject AntPrefab;

        public Product[] ProductsList;
        public BuildingData[] BuildingsList;

        public Dictionary<ProductType, Product> Products { get; } = new Dictionary<ProductType, Product>();
        public Dictionary<BuildingId, BuildingData> Buildings { get; } = new Dictionary<BuildingId, BuildingData>();

        public ProductionManager()
        {
            foreach (ProductType resourceType in Enum.GetValues(typeof(ProductType)).Cast<ProductType>())
            {
                _requestQueues.Add(resourceType, new List<INavigatableProductBuilding>());
                _offerQueues.Add(resourceType, new List<INavigatableProductBuilding>());
            }
        }

        public void MatchProducersAndConsumers()
        {
            foreach (var consumerQueue in _requestQueues)
            {
                FindProducersForConsumers(consumerQueue.Key, consumerQueue.Value);
            }

            foreach (var producerQueue in _offerQueues)
            {
                SendItemsToStorage(producerQueue.Key, producerQueue.Value);
            }
        }

        public void RequestMaterial(ProductType productType, INavigatableProductBuilding consumer)
        {
            if (!TryFindProducer(productType, consumer))
            {
                // No producer found, save the request to the end of the queue for later
                var requestQueue = _requestQueues[productType];
                requestQueue.Add(consumer);
            }
        }

        public void OfferProduct(ProductType productType, INavigatableProductBuilding producer)
        {
            // Is there a consumer available for this resource?
            var consumers = _requestQueues[productType];
            var bestConsumer = SelectBestConsumer(producer, consumers);

            if (bestConsumer != null)
            {
                producer.RemoveProduct(productType);

                SendAnt(productType, bestConsumer.Item2, bestConsumer.Item1);

                consumers.Remove(bestConsumer.Item1);
            }
            // No consumer found, save the resource to the end of the queue for later
            else if (!TrySendToStorage(productType, producer)) 
            {
                // Path to storage not found, save for later
                var offerQueue = _offerQueues[productType];
                offerQueue.Add(producer);
            }
        }

        public void RemoveRequest(ProductType productType, INavigatableProductBuilding consumer)
        {
            var consumers = _requestQueues[productType];
            consumers.Remove(consumer);
        }

        public void RemoveOffer(ProductType productType, INavigatableProductBuilding producer)
        {
            var producers = _offerQueues[productType];
            producers.Remove(producer);
        }

        public void Reset()
        {
            foreach (var requestQueue in _requestQueues.Values)
            {
                requestQueue.Clear();
            }

            foreach (var offerQueue in _offerQueues.Values)
            {
                offerQueue.Clear();
            }
            
            foreach (GameObject ant in _ants)
            {
                Destroy(ant);
            }
            _ants.Clear();
        }

        public void RemoveAnt(GameObject ant)
        {
            Debug.Log("Removing ant, found: " + _ants.Contains(ant));
            _ants.Remove(ant);
        }


        #region Private

        private readonly Dictionary<ProductType, List<INavigatableProductBuilding>> _requestQueues = new Dictionary<ProductType, List<INavigatableProductBuilding>>();
        private readonly Dictionary<ProductType, List<INavigatableProductBuilding>> _offerQueues = new Dictionary<ProductType, List<INavigatableProductBuilding>>();

        private readonly List<GameObject> _ants = new List<GameObject>();

        private void Awake()
        {
            foreach (BuildingData buildingData in BuildingsList)
            {
                Buildings.Add(buildingData.Id, buildingData);
            }

            foreach (Product product in ProductsList)
            {
                Products.Add(product.Type, product);
            }
        }

        private Tuple<INavigatableProductBuilding, IEnumerable<Vector2Int>> SelectBestConsumer(
            INavigatable producer, 
            IEnumerable<INavigatableProductBuilding> consumers)
        {
            foreach (INavigatableProductBuilding consumer in consumers)
            {
                var path = Navigation.FindPath(producer, consumer);
                if (path != null)
                {
                    return new Tuple<INavigatableProductBuilding, IEnumerable<Vector2Int>>(consumer, path);
                }
            }

            // No building reachable
            return null;
        }

        private Tuple<INavigatableProductBuilding, IEnumerable<Vector2Int>> SelectBestProducer(
            INavigatable consumer, 
            IEnumerable<INavigatableProductBuilding> producers)
        {
            INavigatableProductBuilding bestProducer = null;
            IEnumerable<Vector2Int> bestPath = null;
            int shortestDistance = int.MaxValue;
            
            // Find the closest procucer to the consumer
            foreach (INavigatableProductBuilding producer in producers)
            {
                var currentPath = Navigation.FindPath(producer, consumer);
                if (currentPath != null && currentPath.Count < shortestDistance)
                {
                    bestProducer = producer;
                    bestPath = currentPath;
                }
            }

            return 
                bestProducer == null
                ? null 
                : Tuple.Create(bestProducer, bestPath);
        }

        private void SendAnt(ProductType productType, IEnumerable<Vector2Int> path, INavigatableProductBuilding destination)
        {
            var antGo = Instantiate(AntPrefab);
            _ants.Add(antGo);

            var ant = antGo.GetComponent<AntController>();

            ant.Product = productType;
            ant.Destination = destination;

            ant.Navigate(path);
        }

        private void FindProducersForConsumers(ProductType productType, IList<INavigatableProductBuilding> consumers)
        {
            for (int i = consumers.Count - 1; i >= 0; --i)
            {
                var consumer = consumers[i];
                if (TryFindProducer(productType, consumer))
                {
                    consumers.RemoveAt(i);
                }
            }
        }

        private bool TryFindProducer(ProductType productType, INavigatableProductBuilding consumer)
        {
            var producers = _offerQueues[productType];
            var bestProducer = SelectBestProducer(consumer, producers);

            if (bestProducer != null)
            {
                var producer = bestProducer.Item1;
                producer.RemoveProduct(productType);

                SendAnt(productType, bestProducer.Item2, consumer);

                producers.Remove(bestProducer.Item1);
                return true;
            }

            return false;
        }

        private bool TrySendToStorage(ProductType productType, INavigatableProductBuilding producer)
        {
            if (producer.IsStorage)
            {
                return false;
            }

            StorageManager sm = GameManager.Instance.Level.StorageManager;

            var goals = sm.GetRoadsAround();
            var path = Navigation.FindPath(producer, goals);
            if (path != null)
            {
                SendAnt(productType, path, sm);
                producer.RemoveProduct(productType);
                return true;
            }

            return false;
        }

        private void SendItemsToStorage(ProductType productType, IList<INavigatableProductBuilding> producers)
        {
            for (int i = producers.Count - 1; i >= 0; --i)
            {
                var producer = producers[i];
                if (TrySendToStorage(productType, producer))
                {
                    producers.RemoveAt(i);
                }
            }
        }

        #endregion
    }
}
