﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    // TODO fix ant going to not existing building
    public class AntController : MonoBehaviour
    {
        public float Speed = 1;

        public SpriteRenderer PayloadRenderer;

        public float TimeToWaitInFinish = 0.5f;

        public ProductType Product
        {
            get { return _product; }
            set
            {
                PayloadRenderer.sprite = GameManager.Instance.ProductionManager.Products[value].Sprite;
                _product = value;
            }

        }

        public INavigatableProductBuilding Destination
        {
            get { return _destination; }
            set
            {
                if (_destination != null)
                {
                    _destination.OnDestroyed -= HandleTargetDestroyed;
                }
                if (value != null)
                {
                    value.OnDestroyed += HandleTargetDestroyed;
                }

                _destination = value;
            }
        }


        public void Navigate(IEnumerable<Vector2Int> path)
        {
            _path = new Queue<Vector2Int>(path);
            transform.position = PositionUtil.TileToCenterWorld(_path.Dequeue());
            _timeToWait = 0.5f;
        }


        #region Private

        private enum Direction
        {
            None,
            Up, 
            Down,
            Left, 
            Right
        }

        private INavigatableProductBuilding _destination;

        private ProductType _product;

        private Queue<Vector2Int> _path;

        private Animator _animator;

        private Target _target;

        private float _timeToMove;

        private float _timeToWait;

        private Direction _direction = Direction.None;

        private class Target
        {
            public Vector3 Position { get; set; }
            public Vector3 MoveVector { get; set; }
            public float TimeLeft { get; set; }
        }

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _animator.enabled = false;
        }

        private void Start()
        {
            GameManager.Instance.OnGameModeChanged += HandleGameModeChange;
        }

        private void Update ()
        {
            if (GameManager.Instance.IsPaused)
            {
                return;
            }

            _timeToMove = Time.deltaTime;
            MoveToTarget();
            while (_path != null && _timeToMove > 0)
            {
                if (_path.Count == 0)
                {
                    break;
                }
                SetTarget(PositionUtil.TileToCenterWorld(_path.Dequeue()));
                MoveToTarget();
            }

            if (_target == null && _path != null && _path.Count == 0)
            {
                SetDirectionIfDifferent(Direction.None);
                float minTime = Mathf.Min(_timeToWait, _timeToMove);
                _timeToWait -= minTime;
                _timeToMove -= minTime;

                if (_timeToWait <= 0)
                {
                    Destination.ReceiveMaterial(Product);
                    Destroy(gameObject);
                }
            }
        }

        private void HandleTargetDestroyed()
        {
            GameManager.Instance.ProductionManager.RemoveAnt(gameObject);
            Destroy(gameObject);
        }

        private void HandleGameModeChange(GameMode oldMode, GameMode newMode, object args)
        {
            if (GameManager.Instance.IsPaused)
            {
                _animator.enabled = false;
            }
            else if (newMode == GameMode.Play)
            {
                if (_direction != Direction.None)
                {
                    _animator.enabled = true;
                }
            }
        }

        private void SetTarget(Vector3 position)
        {
            var target = new Target
            {
                Position = position
            };
            float distanceLeft = Vector3.Distance(transform.position, position);
            target.TimeLeft = distanceLeft / Speed;
            target.MoveVector = (target.Position - transform.position) / target.TimeLeft;
            _target = target;
            ChangeDirection();
        }

        private void MoveToTarget()
        {
            if (_target == null)
            {
                return;
            }

            if (_timeToMove < _target.TimeLeft)
            {
                transform.position = transform.position + (_target.MoveVector * _timeToMove);
                _target.TimeLeft -= _timeToMove;
                _timeToMove = 0;
            }
            else
            {
                transform.position = _target.Position;
                _timeToMove -= _target.TimeLeft;
                _target = null;
            }
        }

        private void ChangeDirection()
        {
            if (_target == null)
            {
                return;
            }

            if (Mathf.Abs(_target.MoveVector.x) > Mathf.Abs(_target.MoveVector.y))
            {
                if (_target.MoveVector.x > 0)
                {
                    // move right
                    SetDirectionIfDifferent(Direction.Right);
                }
                else
                {
                    // move left
                    SetDirectionIfDifferent(Direction.Left);
                }
            }
            else
            {
                if (_target.MoveVector.y > 0)
                {
                    // move up
                    SetDirectionIfDifferent(Direction.Up);
                }
                else
                {
                    // move down
                    SetDirectionIfDifferent(Direction.Down);
                }
            }
        }

        private void SetDirectionIfDifferent(Direction direction)
        {
            if (_direction == direction)
            {
                return;
            }

            switch (direction)
            {
                case Direction.Up:
                    _animator.Play("AntUp");
                    _animator.enabled = true;
                    break;
                case Direction.Down:
                    _animator.Play("AntDown");
                    _animator.enabled = true;
                    break;
                case Direction.Left:
                    _animator.Play("AntLeft");
                    _animator.enabled = true;
                    break;
                case Direction.Right:
                    _animator.Play("AntRight");
                    _animator.enabled = true;
                    break;
                case Direction.None:
                    _animator.enabled = false;
                    break;
            }

            _direction = direction;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnGameModeChanged -= HandleGameModeChange;
            _destination.OnDestroyed -= HandleTargetDestroyed;
        }

        #endregion
    }
}
