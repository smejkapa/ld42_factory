﻿using UnityEngine;

namespace Scripts
{
    public class CameraManager : MonoBehaviour
    {
        public Level Level;

        public Vector2Int MinPositionDelta;
        public Vector2Int MaxPositionDelta;

        private Vector2 _currentPosDelta;
        private Vector2 _origin;

        private void Start()
        {
            SetupCameraPosition();

            GameManager.Instance.OnLevelRestart += SetupCameraPosition;
        }

        private void SetupCameraPosition()
        {
            _origin = new Vector2(
                (Level.Width - 1) / 2.0f,
                (Level.Height) / 2.0f
            );

            Camera.main.transform.position = new Vector3(
                _origin.x * Level.TILE_DIMENSIONS,
                _origin.y * Level.TILE_DIMENSIONS,
                Camera.main.transform.position.z
            );
        }

        private void Update()
        {
            if (!GameManager.Instance.IsInputEnabled)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _currentPosDelta += Vector2Int.left;
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _currentPosDelta += Vector2Int.up;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _currentPosDelta += Vector2Int.down;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _currentPosDelta += Vector2Int.right;
            }

            _currentPosDelta = new Vector2(
                Mathf.Clamp(_currentPosDelta.x, MinPositionDelta.x, MaxPositionDelta.x),
                Mathf.Clamp(_currentPosDelta.y, MinPositionDelta.y, MaxPositionDelta.y)
            );

            Camera.main.transform.position = new Vector3(
                (_origin.x + _currentPosDelta.x) * Level.TILE_DIMENSIONS,
                (_origin.y + _currentPosDelta.y) * Level.TILE_DIMENSIONS,
                Camera.main.transform.position.z
            );
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnLevelRestart -= SetupCameraPosition;
        }
    }
}
