﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class FactoryController : BuildingController, INavigatableProductBuilding
    {
        public GameObject[] ConsumptionSlots;
        public GameObject[] ProductionSlots;

        public void ReceiveMaterial(ProductType materialType)
        {
            bool isMaterialFound = false;
            foreach (FactoryItem consumingItem in _materials)
            {
                if (!consumingItem.IsReady && 
                    consumingItem.Product.Type == materialType)
                {
                    isMaterialFound = true;
                    consumingItem.ReceiveProduct();
                    break;
                }
            }

            Debug.Assert(isMaterialFound, "Building received a material it has no use for!");
        }

        public void RemoveProduct(ProductType productType)
        {
            bool isProductFound = false;
            foreach (FactoryItem product in _products)
            {
                if (product.IsReady && 
                    product.Product.Type == productType)
                {
                    isProductFound = true;
                    product.ConsumeProduct();
                }
            }

            Debug.Assert(isProductFound, "Building does not have requested product!");
        }

        public bool IsStorage => false;
        public event Action OnDestroyed;

        public List<Vector2Int> GetRoadsAround()
        {
            var level = GameManager.Instance.Level;
            var pathAround = new List<Vector2Int>();
            {
                Vector2Int bottomLeft = BuildingOrigin + new Vector2Int(-1, 0);
                for (int y = 0; y < BuildingData.Height; y++)
                {
                    var v = bottomLeft + new Vector2Int(0, y);
                    if (level.IsInBounds(v) &&
                        level.GetBuildingAt(v)?.BuildingData.BuildingType == BuildingType.Path)
                    {
                        pathAround.Add(v);
                    }

                    v = bottomLeft + new Vector2Int(BuildingData.Width + 1, y);
                    if (level.IsInBounds(v) &&
                        level.GetBuildingAt(v)?.BuildingData.BuildingType == BuildingType.Path)
                    {
                        pathAround.Add(v);
                    }
                }
            }

            {
                var belowOrigin = BuildingOrigin + new Vector2Int(0, -1);
                for (int x = 0; x < BuildingData.Width; x++)
                {
                    var v = belowOrigin + new Vector2Int(x, 0);
                    if (level.IsInBounds(v) &&
                        level.GetBuildingAt(v)?.BuildingData.BuildingType == BuildingType.Path)
                    {
                        pathAround.Add(v);
                    }

                    v = belowOrigin + new Vector2Int(x, BuildingData.Height + 1);
                    if (level.IsInBounds(v) &&
                        level.GetBuildingAt(v)?.BuildingData.BuildingType == BuildingType.Path)
                    {
                        pathAround.Add(v);
                    }
                }
            }

            return pathAround;
        }

        public override void Commit()
        {
            base.Commit();
            foreach (FactoryItem material in _materials)
            {
                GameManager.Instance.ProductionManager.RequestMaterial(material.Product.Type, this);
            }
        }


        #region Private

        private readonly List<FactoryItem> _materials = new List<FactoryItem>();
        private readonly List<FactoryItem> _products = new List<FactoryItem>();

        protected override void Start()
        {
            base.Start();

            Debug.Assert(ConsumptionSlots.Length == BuildingData.Consumes.Length);
            Debug.Assert(ProductionSlots.Length == BuildingData.Produces.Length);

            for (int i = 0; i < ConsumptionSlots.Length; i++)
            {
                //var factoryItemSpriteRenderer = ConsumptionSlots[i].GetComponent<SpriteRenderer>();
                //factoryItemSpriteRenderer.sprite = BuildingData.Consumes[i].Sprite;

                _materials.Add(ConsumptionSlots[i].GetComponent<FactoryItem>());
                _materials[i].Product = BuildingData.Consumes[i];
            }

            for (int i = 0; i < ProductionSlots.Length; i++)
            {
                var factoryItemSpriteRenderer = ProductionSlots[i].GetComponent<SpriteRenderer>();
                factoryItemSpriteRenderer.sprite = BuildingData.Produces[i].Sprite;

                _products.Add(ProductionSlots[i].GetComponent<FactoryItem>());
                _products[i].Product = BuildingData.Produces[i];
            }

            if (GameManager.Instance.GameMode == GameMode.Play)
            {
                Commit();
            }
        }

        protected void Update()
        {
            if (GameManager.Instance.IsPaused)
            {
                return;
            }

            if (AreAllMaterialsReady() && AreAllProductsEmpty())
            {
                StartProduction();
            }
        }

        private bool AreAllMaterialsReady()
        {
            foreach (FactoryItem item in _materials)
            {
                if (!item.IsReady)
                {
                    return false;
                }
            }

            return true;
        }

        private bool AreAllProductsEmpty()
        {
            foreach (FactoryItem product in _products)
            {
                if (product.IsReady || product.IsInProgress)
                {
                    return false;
                }
            }

            return true;
        }

        private void StartProduction()
        {
            foreach (FactoryItem material in _materials)
            {
                material.ConsumeProduct();
                GameManager.Instance.ProductionManager.RequestMaterial(material.Product.Type, this);
            }

            foreach (FactoryItem product in _products)
            {
                product.StartNewProduction(() =>
                {
                    GameManager gm = GameManager.Instance;
                    // Ignore gold, otherwise offer
                    if (product.Product.Type != ProductType.Gold)
                    {
                        gm.ProductionManager.OfferProduct(product.Product.Type, this);
                    }
                    else // Gold is immediately added to the stash
                    {
                        product.ConsumeProduct();
                    }
                    
                    if (BuildingData.GoldProduction > 0)
                    {
                        gm.Level.SellProduct(BuildingData.GoldProduction, BuildingData.Consumes[0].Type, BuildingOrigin);
                    }
                });
            }
        }

        private void OnDisable()
        {
            foreach (FactoryItem product in _products)
            {
                GameManager.Instance.ProductionManager.RemoveOffer(product.Product.Type, this);
            }

            foreach (FactoryItem material in _materials)
            {
                GameManager.Instance.ProductionManager.RemoveRequest(material.Product.Type, this);
            }
        }

        private void OnDestroy()
        {
            OnDestroyed?.Invoke();
        }

        #endregion

    }
}
