﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    class FlyingTextController : MonoBehaviour
    {
        public float FullFadeInSeconds;
        public float FlySpeed;
        public TweenType FadeTweenType;

        public void SetText(string text)
        {
            _text.text = text;
        }

        public void SetTextColor(Color color)
        {
            _text.color = color;
        }
        

        #region Private

        private Text _text;

        private float _alphaProgress = 0.0f;

        private void Awake()
        {
            _text = GetComponentInChildren<Text>();
        }

        private void Update()
        {
            if (_text.color.a < 0.2f)
            {
                Destroy(gameObject);
                return;
            }

            _alphaProgress += Time.deltaTime / FullFadeInSeconds;
            _text.color = new Color(
                _text.color.r,
                _text.color.g,
                _text.color.b,
                Tweens.TweenByType(FadeTweenType, 1.0f, 0.0f, _alphaProgress)
            );

            transform.position = new Vector3(
                transform.position.x,
                transform.position.y + (Time.deltaTime * FlySpeed)
            );
        }

        #endregion
    }
}
