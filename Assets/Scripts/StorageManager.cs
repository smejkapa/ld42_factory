﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts
{
    public class StorageManager : INavigatableProductBuilding
    {
        public void ReceiveMaterial(ProductType materialType)
        {
            var emptyStorage = GetNextEmptyStorage();
            if (emptyStorage == null)
            {
                GameManager.Instance.GameOver();
                Debug.Log("GAME OVER!");
            }
            else
            {
                emptyStorage.ReceiveMaterial(materialType);
                GameManager.Instance.ProductionManager.OfferProduct(materialType, emptyStorage);
            }
        }

        public void RemoveProduct(ProductType productType)
        {
            throw new NotImplementedException();
        }

        public bool IsStorage => true;
        public event Action OnDestroyed;

        public StorageManager(Level level)
        {
            _level = level;
        }

        public List<Vector2Int> GetRoadsAround()
        {
            var storages = _level.GetStorages();
            var paths = new List<Vector2Int>();
            foreach (StorageController storage in storages)
            {
                foreach (var direction in Navigation.DIRECTIONS)
                {
                    var v = storage.BuildingOrigin + direction;
                    if (_level.IsInBounds(v) &&
                        _level.GetBuildingAt(v)?.BuildingData.BuildingType == BuildingType.Path)
                    {
                        paths.Add(v);
                    }
                }
            }

            return paths;
        }


        #region Private

        private readonly Level _level;

        private StorageController GetNextEmptyStorage()
        {
            var storages = _level.GetStorages();
            var orderedStorages = storages.OrderBy(a => a.BuildingOrigin, PositionUtil.PositionComparer);
            foreach (StorageController storage in orderedStorages)
            {
                if (storage.IsEmpty)
                {
                    return storage;
                }
            }

            return null;
        }

        private void OnDestroy()
        {
            OnDestroyed?.Invoke();
        }

        #endregion
    }
}
