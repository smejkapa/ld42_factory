﻿using UnityEngine;

namespace Scripts
{
    [CreateAssetMenu(fileName = "Product", menuName = "AntFactory/Product", order = 2)]
    public class Product : ScriptableObject
    {
        public ProductType Type;
        public float SecondsToProduce;
        public Sprite Sprite;
    }
}
