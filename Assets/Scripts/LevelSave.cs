﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts
{
    [Serializable]
    public class BuildingPositionPair
    {
        public Vector2Int Position;
        public int BuildingId;
    }

    public class VictoryCondition
    {
        public int Product;
        public int Count;
    }

    [Serializable]
    public class LevelSave
    {
        public int StartingGold;
        public int Width;
        public int Height;
        public List<BuildingPositionPair> Buildings;
        public List<VictoryCondition> VictoryConditions;
        public List<int> AvailableBuildings;
    }
}
