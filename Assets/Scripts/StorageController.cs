﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts
{
    public class StorageController : BuildingController, INavigatableProductBuilding
    {
        public FactoryItem Item;

        public bool IsEmpty => !Item.IsReady;

        public void ReceiveMaterial(ProductType materialType)
        {
            Item.Product = GameManager.Instance.ProductionManager.Products[materialType];
            Item.ReceiveProduct();
        }

        public void RemoveProduct(ProductType productType)
        {
            Item.ConsumeProduct();
            Item.Product = null;
        }

        public bool IsStorage => true;
        public event Action OnDestroyed;

        public List<Vector2Int> GetRoadsAround()
        {
            return GameManager.Instance.Level.StorageManager.GetRoadsAround();
        }

        #region Private

        private void Awake()
        {
            Item.Product = null;
        }

        private void OnDestroy()
        {
            OnDestroyed?.Invoke();
        }

        #endregion
    }
}
