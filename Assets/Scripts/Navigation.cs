﻿using System;
using System.Collections.Generic;
using Priority_Queue;
using UnityEngine;

namespace Scripts
{
    public class Navigation
    {
        public static List<Vector2Int> FindPath(INavigatable from, INavigatable to)
        {
            var fromPositions = from.GetRoadsAround();
            var toPositions = to.GetRoadsAround();

            return FindPath(fromPositions, toPositions);
        }

        public static List<Vector2Int> FindPath(INavigatable from, List<Vector2Int> toPositions)
        {
            var fromPositions = from.GetRoadsAround();

            return FindPath(fromPositions, toPositions);
        }

        public static List<Vector2Int> FindPath(List<Vector2Int> fromPositions, List<Vector2Int> toPositions)
        {
            if (fromPositions.Count == 0 || toPositions.Count == 0)
            {
                // no path exists
                return null;
            }

            var navigationData = new NavigationData(GameManager.Instance.Level, fromPositions, toPositions);

            while (!navigationData.IsFinished)
            {
                Node nextNode = navigationData.PriorityQueue.Dequeue();
                SearchNode(navigationData, nextNode);
            }

            return navigationData.BackTrackPath();
        }

        public static readonly Vector2Int[] DIRECTIONS =
        {
            new Vector2Int(0, 1),
            new Vector2Int(1, 0),
            new Vector2Int(0, -1),
            new Vector2Int(-1, 0)
        };


        #region Private

        private static readonly int GOAL = -1;

        private class Node : IComparable<Node>
        {
            public Vector2Int Position { get; }
            public int PathLength { get; }
            private int MinDistanceToGoal { get; }
            private int OptimisticDistanceToGoal => PathLength + MinDistanceToGoal;
            private readonly int _random;

            public Node(Vector2Int position, int pathLength, int minLengthToGoal, int random)
            {
                Position = position;
                PathLength = pathLength;
                MinDistanceToGoal = minLengthToGoal;
                _random = random;
            }

            public int CompareTo(Node other)
            {
                if (OptimisticDistanceToGoal != other.OptimisticDistanceToGoal)
                {
                    return OptimisticDistanceToGoal - other.OptimisticDistanceToGoal;
                }
                else
                {
                    return _random - other._random;
                }
            }
        }

        private class NavigationData
        {
            public Level Level { get; }
            private List<Vector2Int> FromPositions { get; }
            private List<Vector2Int> ToPositions { get; }
            public Dictionary<Vector2Int, int> Distances { get; } = new Dictionary<Vector2Int, int>();
            public readonly SimplePriorityQueue<Node, Node> PriorityQueue = new SimplePriorityQueue<Node, Node>();
            public bool IsFinished => GoalNode != null || PriorityQueue.Count == 0;
            public Node GoalNode;
            private int _order;

            public NavigationData(Level level, List<Vector2Int> fromPositions, List<Vector2Int> toPositions)
            {
                Level = level;
                FromPositions = fromPositions;
                ToPositions = toPositions;

                foreach (Vector2Int startPosition in fromPositions)
                {
                    AddNode(startPosition, 0);
                }

                foreach (Vector2Int toPosition in toPositions)
                {
                    if (Distances.ContainsKey(toPosition))
                    {
                        GoalNode = AddNode(toPosition, 0);
                        return;
                    }
                    else
                    {
                        Distances.Add(toPosition, GOAL);
                    }
                }
            }

            public List<Vector2Int> BackTrackPath()
            {
                if (GoalNode == null)
                {
                    return null;
                }

                var path = new List<Vector2Int>
                {
                    GoalNode.Position
                };
                Vector2Int actualPosition = GoalNode.Position;
                int actualPathLength = GoalNode.PathLength;

                while (actualPathLength != 0)
                {
                    bool found = false;

                    foreach (Vector2Int difference in DIRECTIONS)
                    {
                        Vector2Int position = actualPosition + difference;
                        if (Distances.ContainsKey(position) && actualPathLength - Distances[position] == 1)
                        {
                            actualPosition = position;
                            actualPathLength--;
                            path.Add(actualPosition);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    { 
                        throw new Exception("Ooops. Navigation failed.");
                    }
                }

                path.Reverse();
                return path;
            }

            public Node AddNode(Vector2Int position, int pathLength)
            {
                var newNode = new Node(position, pathLength, CountMinLengthToGoal(position, ToPositions), _order++);
                PriorityQueue.Enqueue(newNode, newNode);
                Distances[position] = newNode.PathLength;
                return newNode;
            }
        }

        private static int CountMinLengthToGoal(Vector2Int from, List<Vector2Int> to)
        {
            int minDistance = int.MaxValue;

            foreach (Vector2Int toPosition in to)
            {
                int distance = Mathf.Abs(from.x - toPosition.x) + Mathf.Abs(from.y - toPosition.y);

                if (distance < minDistance)
                {
                    minDistance = distance;
                }
            }

            return minDistance;
        }

        private static void SearchNode(NavigationData navigationData, Node node)
        {
            if (node.PathLength > navigationData.Distances[node.Position])
            {
                return;
            }

            Level level = navigationData.Level;

            foreach (Vector2Int difference in DIRECTIONS)
            {
                Vector2Int position = node.Position + difference;
                if (!level.IsInBounds(position))
                {
                    continue;
                }

                BuildingController building = level.Tiles[position.x, position.y].Building;
                if (building == null || building.BuildingData.BuildingType != BuildingType.Path)
                {
                    continue;
                }

                if (navigationData.Distances.ContainsKey(position))
                {
                    if (navigationData.Distances[position] == GOAL)
                    {
                        navigationData.GoalNode = navigationData.AddNode(position, node.PathLength + 1);
                        return;
                    }

                    if (navigationData.Distances[position] > node.PathLength + 1)
                    {
                        navigationData.AddNode(position, node.PathLength + 1);
                    }
                }
                else
                {
                    navigationData.AddNode(position, node.PathLength + 1);
                }
            }

        }

        #endregion
    }
}
