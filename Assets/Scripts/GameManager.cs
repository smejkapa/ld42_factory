﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Scripts
{
    public enum InputMode
    {
        None,
        Build,
        Destroy
    }

    public enum GameMode
    {
        Play,
        Pause,
        Menu,
        GameEnd
    }

    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        public bool IsInputEnabled { get; private set; }

        public Level Level;
        public Texture2D DestroyCursor;
        public GameObject PauseHighlightPanel;
        public ProductionManager ProductionManager;
        public GameObject MainMenuPanel;
        public GameObject GameOverPanel;
        public GameObject VictoryPanel;
        public GameObject NextLevelButton;
        public GameObject HelpPanel;

        public string[] Levels;

        public InputMode InputMode { get; private set; }
        public delegate void InputModeChanged(InputMode oldMode, InputMode newMode, object args);
        public event InputModeChanged OnInputModeChanged;

        public GameMode GameMode { get; private set; }
        public delegate void GameModeChanged(GameMode oldMode, GameMode newMode, object args);
        public event GameModeChanged OnGameModeChanged;

        public event Action OnLevelRestart;

        public bool IsPaused => 
            GameMode == GameMode.Pause || 
            GameMode == GameMode.Menu ||
            GameMode == GameMode.GameEnd;

        public void SetBuildMode(BuildingData building)
        {
            SetInputMode(InputMode.Build, building);
        }
        public void SetDestroyMode()
        {
            SetInputMode(InputMode.Destroy, null);
        }
        public void ResetInputMode()
        {
            SetInputMode(InputMode.None, null);
        }

        public void ToggleMenu()
        {
            if (GameMode == GameMode.GameEnd)
            {
                return;
            }

            if (GameMode == GameMode.Menu)
            {
                IsInputEnabled = true;
                SetGameMode(GameMode.Play, null);
                MainMenuPanel.SetActive(false);
            }
            else // Go to menu
            {
                IsInputEnabled = false;
                SetGameMode(GameMode.Menu, null);
                MainMenuPanel.SetActive(true);
            }

            SetInputMode(InputMode.None, null);
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public void GameOver()
        {
            GameOverPanel.SetActive(true);
            SetGameMode(GameMode.GameEnd, null);
        }

        public void Victory()
        {
            VictoryPanel.SetActive(true);
            NextLevelButton.SetActive(_currentLevel < Levels.Length - 1);
            SetGameMode(GameMode.GameEnd, null);
        }

        public void RestartLevel()
        {
            SetSpeed(1.0f);
            IsInputEnabled = true;
            SetGameMode(GameMode.Play, null);
            SetInputMode(InputMode.None, null);

            LoadLevel(Levels[_currentLevel]);

            GameOverPanel.SetActive(false);
            MainMenuPanel.SetActive(false);
            VictoryPanel.SetActive(false);
            HelpPanel.SetActive(false);

            OnLevelRestart?.Invoke();
        }

        public void StartLevel(int number)
        {
            _currentLevel = number;
            RestartLevel();
        }

        public void NextLevel()
        {
            ++_currentLevel;
            RestartLevel();
        }

        public void SetSpeed(float timeScale)
        {
            Time.timeScale = timeScale;
        }


        #region Private

        private int _currentLevel;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }

            IsInputEnabled = true;
            OnInputModeChanged += HandleInputModeChange;
            OnGameModeChanged += HandleGameModeChange;
            PauseHighlightPanel.SetActive(false);
        }

        private void Start()
        {
            LoadLevel(Levels[_currentLevel]);
        }

        private void Update()
        {
            HandleInput();
        }

        private void HandleInput()
        {
            HelpPanel.SetActive(false);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ToggleMenu();
            }

            if (!IsInputEnabled)
            {
                return;
            }

            if (Input.GetButtonDown("CancelGame"))
            {
                SetInputMode(InputMode.None, null);
                Debug.Log("Input mode reset");
            }
            else if (Input.GetButtonDown("PauseGame"))
            {
                if (GameMode == GameMode.Play)
                {
                    SetGameMode(GameMode.Pause, null);
                }
                else
                {
                    SetGameMode(GameMode.Play, null);
                }
            }
#if UNITY_EDITOR
            else if (Input.GetKeyDown(KeyCode.S))
            {
                SaveLevel();
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                LoadLevel("LevelSave");
            }
#endif
            else if (Input.GetKey(KeyCode.H))
            {
                HelpPanel.SetActive(true);
            }
        }

        private void SaveLevel()
        {
            // Save the level
            var save = Level.GenerateSave();

            if (save.VictoryConditions == null)
            {
                // Add dummy victory condition for easier editing
                save.VictoryConditions = new List<VictoryCondition>
                {
                    new VictoryCondition
                    {
                        Count = 123,
                        Product = (int) ProductType.None
                    }
                };
            }

            if (save.AvailableBuildings == null)
            {
                // Add dummy buildings for easier editing
                save.AvailableBuildings = new List<int>
                {
                    (int) BuildingId.LeafStickFactory, (int)BuildingId.StatueFactory
                };
            }

            // Serialize
            var serializer = new XmlSerializer(typeof(LevelSave));
            using (var sw = new StreamWriter(Path.Combine(Application.dataPath, "Resources", "Levels", "LevelSave.xml")))
            {
                using (var xmlWriter = new XmlTextWriter(sw))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    serializer.Serialize(xmlWriter, save);
                    Debug.Log("Level saved");
                }
            }
        }

        private void LoadLevel(string lvlName)
        {
            var serialier = new XmlSerializer(typeof(LevelSave));

            var file = Resources.Load<TextAsset>(Path.Combine("Levels", $"{lvlName}"));

            using (var sr = new StringReader(file.text))
            {
                using (var xmlreader = new XmlTextReader(sr))
                {
                    var save = (LevelSave)serialier.Deserialize(xmlreader);
                    Level.LoadFromSave(save);
                    Debug.Log("Level loaded");
                }
            }
            ProductionManager.Reset();
        }

        private void SetInputMode(InputMode newMode, object args)
        {
            InputMode oldMode = InputMode;
            InputMode = newMode;
            OnInputModeChanged?.Invoke(oldMode, InputMode, args);
        }

        private void SetGameMode(GameMode newMode, object args)
        {
            GameMode oldMode = GameMode;
            GameMode = newMode;
            OnGameModeChanged?.Invoke(oldMode, GameMode, args);
        }

        private void HandleInputModeChange(InputMode oldMode, InputMode newMode, object args)
        {
            if (newMode == InputMode.Destroy)
            {
                Cursor.SetCursor(DestroyCursor, Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            }
        }

        private void HandleGameModeChange(GameMode oldMode, GameMode newMode, object args)
        {
            PauseHighlightPanel.SetActive(GameMode == GameMode.Pause);
        }

        #endregion
    }
}
