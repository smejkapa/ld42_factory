﻿using UnityEngine;

namespace Scripts
{
    public class Tile
    {
        public bool IsBuildable => Building == null;
        public GameObject TileObject { get; set; }
        public BuildingController Building { get; set; }
    }
}
