﻿Shader "AntFactory/Progress"
{
	Properties
    {
		_MainTex ("Texture", 2D) = "white" {}
        _ColorDone ("Done color", Color) = (0, 1, 0, 1)
        _ColorRemaining ("Remaining color", Color) = (0.8, 0.8, 0.8, 1)
        _ColorBackground ("Background color", Color) = (1, 1, 1, 1)
        _ColorInactive ("Inactive color", Color) = (1, 0, 0, 1)
        _Progress ("Progress", Float) = 0.0
        _IsActive ("IsActive", Int) = 0
        _ItemOpacity ("Item opacity", Float) = 0.5
	}

    CGINCLUDE
    #include "UnityCG.cginc"

    struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };

    struct v2f
    {
        float4 vertex : SV_POSITION;
        float2 uv : TEXCOORD0;
    };

    sampler2D _MainTex;
    float4 _MainTex_ST;
    float _Progress;
    fixed4 _ColorDone;
    fixed4 _ColorRemaining;
    fixed4 _ColorBackground;
    fixed4 _ColorInactive;
    float _ItemOpacity;
    int _IsActive;

    v2f vert(appdata v)
    {
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.uv = TRANSFORM_TEX(v.uv, _MainTex);

        return o;
    }

    fixed4 frag(v2f i) : SV_Target
    {
        fixed4 col = tex2D(_MainTex, i.uv);
        if (col.a < 0.01)
        {
            if (_Progress <= 0.0)
            {
                col = _ColorInactive;
            }
            else if (i.uv.y > _Progress)
            {
                col = _ColorRemaining;
            }
            else
            {
                col = _ColorDone;
            }
        }
        else
        {
            if (i.uv.y > _Progress)
            {
                // Custom alpha blending
                col.rgb = col.rgb * _ItemOpacity + _ColorBackground.rgb * (1 - _ItemOpacity);
            }
        }

        return col;
    }
    ENDCG

    SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        //Tags { "RenderType"="Opaque" }

		Pass
		{
            Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
            ENDCG
		}
	}
}
